// 显示loading
export const ml_showloading =  () => {
  return new Promise((resolve) => {
    wx.showLoading({
      title: 'loading......',
      success: resolve
    })
  })
}
//隐藏loading
export const ml_hideloading = () => {
  return new Promise((resolve) => {
    wx.hideLoading({
      success: resolve
    })
  })
}
//消息提示框
export const ml_showtoast = (title) => {
  return new Promise((resolve) => {
    wx.showToast({
      title,
      icon:'none',
      success:resolve
    })
  })
}
//消息提示框成功
export const ml_showtoastSuccess = (title) => {
  return new Promise((resolve) => {
    wx.showToast({
      title,
      success:resolve
    })
  })
}
//发起支付
export const ml_payment = (pay) => {
  return new Promise((resolve,reject) => {
    wx.requestPayment({
      ...pay,
      success:resolve,
      fail:reject
    })
  })
}