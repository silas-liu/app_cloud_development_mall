// 云函数入口文件

const cloud = require('wx-server-sdk')

//指定环境的数据库
const db = cloud.database({
  env:'store-zaj0y'
})
const order_col = db.collection('orders')
cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {

  //创建随机订单号
  let object = {
    order_num : Date.now(),
    carts:event.carts,
    status:0
  }
//添加到orders数据库
let res = await  order_col.add({
  data:object
})
return {
  res,
  order_num:object.order_num
}
}