// 云函数入口文件
const cloud = require('wx-server-sdk')
const db = cloud.database({
  env:'store-zaj0y'
})
const orders_col = db.collection('orders')
cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {

    let res = await orders_col.where({
      order_number:event.order_number
    }).update({
      data:{
        status:1
      }
    })
    return res
}