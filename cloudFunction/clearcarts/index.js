// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db = cloud.database({
  env:'store-zaj0y'
})
const order_col = db.collection('orders')
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
let res = await order_col.where({
  status:1
}).remove()
  return res
}