const db = wx.cloud.database()
const goods_col = db.collection('goods')


Page({
  data: {
    detail: {}
  },

  onLoad: function (options) {
    console.log(options)
    let {id} = options
    this.loadDetailData(id)
  },
  //加载详情数据函数
async loadDetailData(id) {
  //拿到对应id的数据
  let ins = goods_col.doc(id)
  //更新访问次数
  await ins.update({
    data:{
      count:db.command.inc(1)
    }
  })
  //获取最新数据
  let res = await ins.get()
  //赋值
  this.setData({
    detail:res.data
  })
}
})

